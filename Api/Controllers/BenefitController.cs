using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Domain.Interfaces.Repositories;
using AutoMapper;
using Application.Dtos;
using FluentValidation.Results;
using Application.Validation;
using Domain.Helpers;
using Api.Extensions;

namespace Api.Controllers;


[ApiController]
[Route("[controller]")]
public class BenefitController : ControllerBase
{

    private readonly IBenefitsRepository benefitRepository;
    private readonly IMapper mapper;

    public BenefitController(IBenefitsRepository benefitRepository, IMapper mapper)
    {
        this.benefitRepository = benefitRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<Benefit> pagedBenefitsList = await benefitRepository.GetAll(paginationParams);
        List<GetAllBenefitsResponseDto> benefitsResponseDto = mapper.Map<List<GetAllBenefitsResponseDto>>(pagedBenefitsList);

        Response.AddPaginationHeader(new PaginationHeader(pagedBenefitsList.CurrentPage, pagedBenefitsList.PageSize,
               pagedBenefitsList.TotalCount, pagedBenefitsList.TotalPages));

        return Ok(benefitsResponseDto);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetBenefit(int id)
    {
        Benefit? skill = await benefitRepository.GetOne(id);
        GetBenefitResponseDto benefitDto = mapper.Map<GetBenefitResponseDto>(skill);

        if (benefitDto is null)
        {
            return NotFound();
        }
        return Ok(benefitDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreateBenefits([FromBody] CreateBenefitRequestDto createBenefitRequestDto)
    {
        createBenefitRequestDto.Name = createBenefitRequestDto.Name.Trim();

        CreateBenefitValidator validator = new CreateBenefitValidator(benefitRepository);
        ValidationResult valResult = validator.Validate(createBenefitRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Benefit createBenefit = mapper.Map<Benefit>(createBenefitRequestDto);

        Benefit? responseBenefit = await benefitRepository.Insert(createBenefit);
        CreateBenefitResponseDto createdBenefit = mapper.Map<CreateBenefitResponseDto>(responseBenefit);


        return Ok(createdBenefit);
    }

    [HttpPut]
    public async Task<IActionResult> EditBenefits([FromBody] EditBenefitRequestDto editbenefitRequestDto)
    {
        editbenefitRequestDto.Name = editbenefitRequestDto.Name.Trim();

        EditBenefitValidator validator = new EditBenefitValidator(benefitRepository);
        ValidationResult valResult = validator.Validate(editbenefitRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Benefit benefit = mapper.Map<Benefit>(editbenefitRequestDto);



        await benefitRepository.Edit(benefit);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteBenefit(int id)
    {
        Benefit? benefitToDelete = await benefitRepository.GetOne(id);
        if (benefitToDelete is null)
        {
            return NotFound();
        }
        await benefitRepository.Delete(benefitToDelete);
        return NoContent();
    }




}