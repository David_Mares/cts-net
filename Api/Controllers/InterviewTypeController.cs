using Api.Extensions;
using Application.Dtos;
using Application.Validation;
using AutoMapper;
using Domain.Entities;
using Domain.Helpers;
using Domain.Interfaces.Repositories;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class InterviewTypeController : ControllerBase
{

    private readonly IInterviewTypesRepository interviewTypesRepository;
    private readonly IMapper mapper;
    public InterviewTypeController(IInterviewTypesRepository interviewTypesRepository, IMapper mapper)
    {
        this.interviewTypesRepository = interviewTypesRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<InterviewType> pagedInterviewTypesList = await interviewTypesRepository.GetAll(paginationParams);
        List<GetAllInterviewTypesResponseDto> interviewTypesResponseDto = mapper.Map<List<GetAllInterviewTypesResponseDto>>(pagedInterviewTypesList);

        Response.AddPaginationHeader(new PaginationHeader(pagedInterviewTypesList.CurrentPage, pagedInterviewTypesList.PageSize,
               pagedInterviewTypesList.TotalCount, pagedInterviewTypesList.TotalPages));

        return Ok(interviewTypesResponseDto);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetInterviewType(int id)
    {
        InterviewType? interviewType = await interviewTypesRepository.GetOne(id);
        GetInterviewTypeResponseDto interviewTypeDto = mapper.Map<GetInterviewTypeResponseDto>(interviewType);

        if (interviewTypeDto is null)
        {
            return NotFound();
        }
        return Ok(interviewTypeDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreateInterviewTypes([FromBody] CreateInterviewTypeRequestDto createInterviewTypeRequestDto)
    {
        createInterviewTypeRequestDto.Description = createInterviewTypeRequestDto.Description.Trim();

        CreateInterviewTypeValidator validator = new CreateInterviewTypeValidator(interviewTypesRepository);
        ValidationResult valResult = validator.Validate(createInterviewTypeRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        InterviewType createInterviewType = mapper.Map<InterviewType>(createInterviewTypeRequestDto);

        InterviewType? responseInterviewType = await interviewTypesRepository.Insert(createInterviewType);
        CreateInterviewTypeResponseDto createdInterviewType = mapper.Map<CreateInterviewTypeResponseDto>(responseInterviewType);


        return Ok(createdInterviewType);
    }

    [HttpPut]
    public async Task<IActionResult> EditInterviewTypes([FromBody] EditInterviewTypeRequestDto editInterviewTypeRequestDto)
    {
        editInterviewTypeRequestDto.Description = editInterviewTypeRequestDto.Description.Trim();

        EditInterviewTypeValidator validator = new EditInterviewTypeValidator(interviewTypesRepository);
        ValidationResult valResult = validator.Validate(editInterviewTypeRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        InterviewType interviewType = mapper.Map<InterviewType>(editInterviewTypeRequestDto);



        await interviewTypesRepository.Edit(interviewType);
        return NoContent();
    }



    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSeniority(int id)
    {
        InterviewType? interviewTypeToDelete = await interviewTypesRepository.GetOne(id);
        if (interviewTypeToDelete == null)
        {
            return NotFound();
        }
        await interviewTypesRepository.Delete(interviewTypeToDelete);
        return NoContent();
    }




}