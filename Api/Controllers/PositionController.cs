using Api.Extensions;
using Application.Dtos;
using Application.Validation;
using AutoMapper;
using Domain.Entities;
using Domain.Helpers;
using Domain.Interfaces.Repositories;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class PositionController : ControllerBase
{

    private readonly IPositionsRepository positionsRepository;
    private readonly IMapper mapper;
    public PositionController(IPositionsRepository positionsRepository, IMapper mapper)
    {
        this.positionsRepository = positionsRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<Position> pagedPositionsList = await positionsRepository.GetAll(paginationParams);
        List<GetAllPositionsResponseDto> positionsResponseDto = mapper.Map<List<GetAllPositionsResponseDto>>(pagedPositionsList);

        Response.AddPaginationHeader(new PaginationHeader(pagedPositionsList.CurrentPage, pagedPositionsList.PageSize,
               pagedPositionsList.TotalCount, pagedPositionsList.TotalPages));

        return Ok(positionsResponseDto);
    }

     [HttpGet("{id}")]
    public async Task<IActionResult> GetPosition(int id)
    {
        Position? position = await positionsRepository.GetOne(id);
        GetPositionResponseDto positionDto = mapper.Map<GetPositionResponseDto>(position);

        if (positionDto is null)
        {
            return NotFound();
        }
        return Ok(positionDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreatePositions([FromBody] CreatePositionRequestDto createPositionRequestDto)
    {
        createPositionRequestDto.Name = createPositionRequestDto.Name.Trim();

        CreatePositionValidator validator = new CreatePositionValidator(positionsRepository);
        ValidationResult valResult = validator.Validate(createPositionRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Position createPosition = mapper.Map<Position>(createPositionRequestDto);

        Position? responsePosition = await positionsRepository.Insert(createPosition);
        CreatePositionResponseDto createdPosition = mapper.Map<CreatePositionResponseDto>(responsePosition);


        return Ok(createdPosition);
    }

    [HttpPut]
    public async Task<IActionResult> EditPositions([FromBody] EditPositionRequestDto editPositionRequestDto)
    {
        editPositionRequestDto.Name = editPositionRequestDto.Name.Trim();

        EditPositionValidator validator = new EditPositionValidator(positionsRepository);
        ValidationResult valResult = validator.Validate(editPositionRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Position Position = mapper.Map<Position>(editPositionRequestDto);



        await positionsRepository.Edit(Position);
        return NoContent();
    }



    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePosition(int id)
    {
        Position? PositionToDelete = await positionsRepository.GetOne(id);
        if (PositionToDelete == null)
        {
            return NotFound();
        }
        await positionsRepository.Delete(PositionToDelete);
        return NoContent();
    }



}