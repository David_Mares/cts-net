using Api.Extensions;
using Application.Dtos;
using Application.Validation;
using AutoMapper;
using Domain.Entities;
using Domain.Helpers;
using Domain.Interfaces.Repositories;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class SeniorityController : ControllerBase
{

    private readonly ISenioritiesRepository senioritiesRepository;
    private readonly IMapper mapper;
    public SeniorityController(ISenioritiesRepository senioritiesRepository, IMapper mapper)
    {
        this.senioritiesRepository = senioritiesRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<Seniority> pagedSenioritiesList = await senioritiesRepository.GetAll(paginationParams);
        List<GetAllSenioritiesResponseDto> senioritiesResponseDto = mapper.Map<List<GetAllSenioritiesResponseDto>>(pagedSenioritiesList);

        Response.AddPaginationHeader(new PaginationHeader(pagedSenioritiesList.CurrentPage, pagedSenioritiesList.PageSize,
               pagedSenioritiesList.TotalCount, pagedSenioritiesList.TotalPages));

        return Ok(senioritiesResponseDto);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetSeniority(int id)
    {
        Seniority? seniority = await senioritiesRepository.GetOne(id);
        GetSeniorityResponseDto seniorityDto = mapper.Map<GetSeniorityResponseDto>(seniority);

        if (seniorityDto is null)
        {
            return NotFound();
        }
        return Ok(seniorityDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreateSeniorities([FromBody] CreateSeniorityRequestDto createSeniorityRequestDto)
    {
        createSeniorityRequestDto.Description = createSeniorityRequestDto.Description.Trim();

        CreateSeniorityValidator validator = new CreateSeniorityValidator(senioritiesRepository);
        ValidationResult valResult = validator.Validate(createSeniorityRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Seniority createSeniority = mapper.Map<Seniority>(createSeniorityRequestDto);

        Seniority? responseSeniority = await senioritiesRepository.Insert(createSeniority);
        CreateSeniorityResponseDto createdSeniority = mapper.Map<CreateSeniorityResponseDto>(responseSeniority);


        return Ok(createdSeniority);
    }

    [HttpPut]
    public async Task<IActionResult> EditSeniorities([FromBody] EditSeniorityRequestDto editSeniorityRequestDto)
    {
        editSeniorityRequestDto.Description = editSeniorityRequestDto.Description.Trim();

        EditSeniorityValidator validator = new EditSeniorityValidator(senioritiesRepository);
        ValidationResult valResult = validator.Validate(editSeniorityRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Seniority seniority = mapper.Map<Seniority>(editSeniorityRequestDto);



        await senioritiesRepository.Edit(seniority);
        return NoContent();
    }



    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSeniority(int id)
    {
        Seniority? seniorityToDelete = await senioritiesRepository.GetOne(id);
        if (seniorityToDelete == null)
        {
            return NotFound();
        }
        await senioritiesRepository.Delete(seniorityToDelete);
        return NoContent();
    }


}