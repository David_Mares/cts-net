using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Domain.Interfaces.Repositories;
using AutoMapper;
using Application.Dtos;
using FluentValidation.Results;
using Application.Validation;
using Domain.Helpers;
using Api.Extensions;

namespace Api.Controllers;


[ApiController]
[Route("[controller]")]
public class SkillController : ControllerBase
{

    private readonly ISkillsRepository skillRepository;
    private readonly IMapper mapper;

    public SkillController(ISkillsRepository skillRepository, IMapper mapper)
    {
        this.skillRepository = skillRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<Skill> pagedSkillsList = await skillRepository.GetAll(paginationParams);
        List<GetAllSkillsResponseDto> skillsResponseDto = mapper.Map<List<GetAllSkillsResponseDto>>(pagedSkillsList);

        Response.AddPaginationHeader(new PaginationHeader(pagedSkillsList.CurrentPage, pagedSkillsList.PageSize,
               pagedSkillsList.TotalCount, pagedSkillsList.TotalPages));

        return Ok(skillsResponseDto);
    }


    [HttpGet("{id}")]
    public async Task<IActionResult> GetSkill(int id)
    {
        Skill? skill = await skillRepository.GetOne(id);
        GetSkillResponseDto skillDto = mapper.Map<GetSkillResponseDto>(skill);

        if (skillDto is null)
        {
            return NotFound();
        }
        return Ok(skillDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreateSkills([FromBody] CreateSkillRequestDto createSkillRequestDto)
    {
        createSkillRequestDto.Description = createSkillRequestDto.Description.Trim();

        CreateSkillValidator validator = new CreateSkillValidator(skillRepository);
        ValidationResult valResult = validator.Validate(createSkillRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Skill createSkill = mapper.Map<Skill>(createSkillRequestDto);

        Skill? responseSkill = await skillRepository.Insert(createSkill);
        CreateSkillResponseDto createdSkill = mapper.Map<CreateSkillResponseDto>(responseSkill);


        return Ok(createdSkill);
    }

    [HttpPut]
    public async Task<IActionResult> EditSkills([FromBody] EditSkillRequestDto editSkillRequestDto)
    {
        editSkillRequestDto.Description = editSkillRequestDto.Description.Trim();

        EditSkillValidator validator = new EditSkillValidator(skillRepository);
        ValidationResult valResult = validator.Validate(editSkillRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        Skill skill = mapper.Map<Skill>(editSkillRequestDto);



        await skillRepository.Edit(skill);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSkill(int id)
    {
        Skill? skillToDelete = await skillRepository.GetOne(id);
        if (skillToDelete == null)
        {
            return NotFound();
        }
        await skillRepository.Delete(skillToDelete);
        return NoContent();
    }



}