using Api.Extensions;
using Application.Dtos;
using Application.Validation;
using AutoMapper;
using Domain.Entities;
using Domain.Helpers;
using Domain.Interfaces.Repositories;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{

    private readonly IUsersRepository usersRepository;
    private readonly IPositionsRepository positionsRepository;
    private readonly IMapper mapper;
    public UserController(IUsersRepository usersRepository,IPositionsRepository positionsRepository, IMapper mapper)
    {
        this.usersRepository = usersRepository;
        this.positionsRepository = positionsRepository;
        this.mapper = mapper;
    }


    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams)
    {

        PagedList<User> pagedUsersList = await usersRepository.GetAll(paginationParams);
        List<GetAllUsersResponseDto> usersResponseDto = mapper.Map<List<GetAllUsersResponseDto>>(pagedUsersList);

        Response.AddPaginationHeader(new PaginationHeader(pagedUsersList.CurrentPage, pagedUsersList.PageSize,
               pagedUsersList.TotalCount, pagedUsersList.TotalPages));

        return Ok(usersResponseDto);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUser(int id)
    {
        User? user = await usersRepository.GetOne(id);
        GetUserResponseDto userDto = mapper.Map<GetUserResponseDto>(user);

        if (userDto is null)
        {
            return NotFound();
        }

        return Ok(userDto);
    }

    [HttpPost]
    public async Task<IActionResult> CreateUsers([FromBody] CreateUserRequestDto createUserRequestDto)
    {
        // createUserRequestDto.Name = createUserRequestDto.Name.Trim();

        CreateUserValidator validator = new CreateUserValidator(usersRepository);
        ValidationResult valResult = validator.Validate(createUserRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }
        User createUser = mapper.Map<User>(createUserRequestDto);

        createUser.Roles = await usersRepository.GetRoles(createUserRequestDto.RoleIds);
        createUser.Positions = await usersRepository.GetPositions(createUserRequestDto.PositionIds);
        User? responseUser = await usersRepository.Insert(createUser);
        CreateUserResponseDto createdUser = mapper.Map<CreateUserResponseDto>(responseUser);


        return Ok(createdUser);
    }

    [HttpPut]
    public async Task<IActionResult> EditUsers([FromBody] EditUserRequestDto editUserRequestDto)
    {
        // editUserRequestDto.Name = editUserRequestDto.Name.Trim();

        EditUserValidator validator = new EditUserValidator(usersRepository);
        ValidationResult valResult = validator.Validate(editUserRequestDto);

        if (!valResult.IsValid)
        {
            return StatusCode(StatusCodes.Status400BadRequest, valResult);
        }

        User userToEdit = mapper.Map<User>(editUserRequestDto);
        userToEdit.Roles = await usersRepository.GetRoles(editUserRequestDto.RoleIds);
        userToEdit.Positions = await usersRepository.GetPositions(editUserRequestDto.PositionIds);

        await usersRepository.Edit(userToEdit);
        return NoContent();
    }


    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteUser(int id)
    {
        User? UserToDelete = await usersRepository.GetOne(id);
        if (UserToDelete == null)
        {
            return NotFound();
        }
        await usersRepository.Delete(UserToDelete);
        return NoContent();
    }
}