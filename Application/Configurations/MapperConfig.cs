using AutoMapper;
using Domain.Entities;
using Application.Dtos;

namespace Application.Configurations;

public class MapperConfig : Profile
{
    public MapperConfig()
    {
        CreateMap<Skill, GetAllSkillsResponseDto>();
        CreateMap<Skill, GetSkillResponseDto>();
        CreateMap<Skill, CreateSkillResponseDto>().ReverseMap();
        CreateMap<CreateSkillRequestDto, Skill>();
        CreateMap<EditSkillRequestDto, Skill>();

        CreateMap<Benefit, GetAllBenefitsResponseDto>();
        CreateMap<Benefit, GetBenefitResponseDto>();
        CreateMap<Benefit, CreateBenefitResponseDto>().ReverseMap();
        CreateMap<CreateBenefitRequestDto, Benefit>();
        CreateMap<EditBenefitRequestDto, Benefit>();

        CreateMap<Seniority, GetAllSenioritiesResponseDto>();
        CreateMap<Seniority, GetSeniorityResponseDto>();
        CreateMap<Seniority, CreateSeniorityResponseDto>().ReverseMap();
        CreateMap<CreateSeniorityRequestDto, Seniority>();
        CreateMap<EditSeniorityRequestDto, Seniority>();

        CreateMap<InterviewType, GetAllInterviewTypesResponseDto>();
        CreateMap<InterviewType, GetInterviewTypeResponseDto>();
        CreateMap<InterviewType, CreateInterviewTypeResponseDto>().ReverseMap();
        CreateMap<CreateInterviewTypeRequestDto, InterviewType>();
        CreateMap<EditInterviewTypeRequestDto, InterviewType>();

        CreateMap<Position, GetAllPositionsResponseDto>();
        CreateMap<Position, GetPositionResponseDto>();
        CreateMap<Position, CreatePositionResponseDto>().ReverseMap();
        CreateMap<CreatePositionRequestDto, Position>();
        CreateMap<EditPositionRequestDto, Position>();


        CreateMap<User, GetAllUsersResponseDto>();
        CreateMap<User, GetUserResponseDto>()
       .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles))
       .ForMember(dest => dest.Positions, opt => opt.MapFrom(src => src.Positions));

        CreateMap<Position, PositionDto>();
        CreateMap<Role, RoleDto>();

        CreateMap<User, CreateUserResponseDto>().ReverseMap();
        CreateMap<CreateUserRequestDto, User>();
        CreateMap<EditUserRequestDto, User>();


    }
}
