using Domain.Entities;

namespace Application.Dtos;

public class CreateBenefitRequestDto
{
    public string Name { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;

    public MeasurementUnit? Unit { get; set; } = null;



}