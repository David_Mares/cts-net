namespace Application.Dtos;

public class CreateInterviewTypeRequestDto
{
    public string Description { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;


}