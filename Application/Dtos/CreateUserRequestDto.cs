using Domain.Entities;

namespace Application.Dtos;

public class CreateUserRequestDto
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public List<int>? PositionIds { get; set; }
    public List<int>? RoleIds { get; set; }
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;


}