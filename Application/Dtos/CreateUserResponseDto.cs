using Domain.Entities;

namespace Application.Dtos;

public class CreateUserResponseDto
{
    public int Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
   public virtual List<Position>? Positions { get; set; }
    public virtual List<Role>? Roles { get; set; }
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = false;


}