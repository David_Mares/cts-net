namespace Application.Dtos;

public class EditSkillRequestDto
{
    public int Id { get; set; }
    public string Description { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = false;


}