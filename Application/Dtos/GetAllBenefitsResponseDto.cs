namespace Application.Dtos;

public class GetAllBenefitsResponseDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = false;


}