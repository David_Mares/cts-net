namespace Application.Dtos;

public class GetAllPositionsResponseDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = false;


}