
namespace Application.Dtos;

public class GetAllUsersResponseDto
{
    public int Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;

    public virtual List<PositionDto>? Positions { get; set; }
    public virtual List<RoleDto>? Roles { get; set; }
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;


}