using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreateBenefitValidator : AbstractValidator<CreateBenefitRequestDto>
{
    private readonly IBenefitsRepository benefitRepository;



    public CreateBenefitValidator(IBenefitsRepository benefitRepository)
    {
        this.benefitRepository = benefitRepository;

        RuleFor(model => model.Name).NotEmpty().WithMessage("Please specify a name");
        RuleFor(x => x.Name)
           .Must((name) =>
            {
                Benefit? existingBenefit = benefitRepository.GetByName(name);

                if (existingBenefit == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
