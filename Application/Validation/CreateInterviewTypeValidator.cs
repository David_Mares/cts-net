using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreateInterviewTypeValidator : AbstractValidator<CreateInterviewTypeRequestDto>
{
    private readonly IInterviewTypesRepository interviewTypesRepository;



    public CreateInterviewTypeValidator(IInterviewTypesRepository interviewTypesRepository)
    {
        this.interviewTypesRepository = interviewTypesRepository;

        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(x => x.Description)
           .Must((description) =>
            {
                InterviewType? existingDescription = interviewTypesRepository.GetByDescription(description);

                if (existingDescription == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
