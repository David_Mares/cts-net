using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreatePositionValidator : AbstractValidator<CreatePositionRequestDto>
{
    private readonly IPositionsRepository positionsRepository;



    public CreatePositionValidator(IPositionsRepository positionsRepository)
    {
        this.positionsRepository = positionsRepository;

        RuleFor(model => model.Name).NotEmpty().WithMessage("Please specify a Name");
        RuleFor(x => x.Name)
           .Must((Name) =>
            {
                Position? existingName = positionsRepository.GetByName(Name);

                if (existingName == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
