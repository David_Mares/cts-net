using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreateSeniorityValidator : AbstractValidator<CreateSeniorityRequestDto>
{
    private readonly ISenioritiesRepository seniorityRepository;



    public CreateSeniorityValidator(ISenioritiesRepository seniorityRepository)
    {
        this.seniorityRepository = seniorityRepository;

        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(x => x.Description)
           .Must((description) =>
            {
                Seniority? existingDescription = seniorityRepository.GetByDescription(description);

                if (existingDescription == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
