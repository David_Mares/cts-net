using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreateSkillValidator : AbstractValidator<CreateSkillRequestDto>
{
    private readonly ISkillsRepository skillRepository;



    public CreateSkillValidator(ISkillsRepository skillRepository)
    {
        this.skillRepository = skillRepository;

        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(x => x.Description)
           .Must((description) =>
            {
                Skill? existingDescription = skillRepository.GetByDescription(description);

                if (existingDescription == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
