using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class CreateUserValidator : AbstractValidator<CreateUserRequestDto>
{
    private readonly IUsersRepository usersRepository;



    public CreateUserValidator(IUsersRepository usersRepository)
    {
        this.usersRepository = usersRepository;

        RuleFor(model => model.LastName).NotEmpty().WithMessage("Please specify a LastName");
        RuleFor(x => x.LastName)
           .Must((Name) =>
            {
                User? existingName = usersRepository.GetByName(Name);

                if (existingName == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
