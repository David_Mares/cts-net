using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditBenefitValidator : AbstractValidator<EditBenefitRequestDto>
{
    private readonly IBenefitsRepository benefitRepository;


    public EditBenefitValidator(IBenefitsRepository benefitRepository)
    {
        this.benefitRepository = benefitRepository;



        RuleFor(model => model.Name).NotEmpty().WithMessage("Please specify a name");
        RuleFor(model => model.Name)
           .Must((model, name) =>
            {

                Benefit? existingName = benefitRepository.GetByName(name);

                if (existingName == null || existingName?.Id == model.Id)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
