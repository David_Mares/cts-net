using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditInterviewTypeValidator : AbstractValidator<EditInterviewTypeRequestDto>
{
    private readonly IInterviewTypesRepository interviewTypesRepository;


    public EditInterviewTypeValidator(IInterviewTypesRepository interviewTypesRepository)
    {
        this.interviewTypesRepository = interviewTypesRepository;



        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(model => model.Description)
           .Must((model, description) =>
            {

                InterviewType? existingDescription = interviewTypesRepository.GetByDescription(description);

                if (existingDescription == null || existingDescription?.Id == model.Id)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
