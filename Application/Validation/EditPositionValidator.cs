using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditPositionValidator : AbstractValidator<EditPositionRequestDto>
{
    private readonly IPositionsRepository positionRepository;


    public EditPositionValidator(IPositionsRepository positionRepository)
    {
        this.positionRepository = positionRepository;

        RuleFor(model => model.Name).NotEmpty().WithMessage("Please specify a Name");
        RuleFor(x => x.Name)
           .Must((Name) =>
            {
                Position? existingName = positionRepository.GetByName(Name);

                if (existingName == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
