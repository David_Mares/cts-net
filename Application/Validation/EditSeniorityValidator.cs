using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditSeniorityValidator : AbstractValidator<EditSeniorityRequestDto>
{
    private readonly ISenioritiesRepository seniorityRepository;


    public EditSeniorityValidator(ISenioritiesRepository seniorityRepository)
    {
        this.seniorityRepository = seniorityRepository;



        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(model => model.Description)
           .Must((model, description) =>
            {

                Seniority? existingDescription = seniorityRepository.GetByDescription(description);

                if (existingDescription == null || existingDescription?.Id == model.Id)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
