using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditSkillValidator : AbstractValidator<EditSkillRequestDto>
{
    private readonly ISkillsRepository skillRepository;


    public EditSkillValidator(ISkillsRepository skillRepository)
    {
        this.skillRepository = skillRepository;



        RuleFor(model => model.Description).NotEmpty().WithMessage("Please specify a description");
        RuleFor(model => model.Description)
           .Must((model, description) =>
            {

                Skill? existingDescription = skillRepository.GetByDescription(description);

                if (existingDescription == null || existingDescription?.Id == model.Id)
                {
                    return true;
                }

                return false;
            }).WithMessage("Description must be unique");
    }

}
