using Application.Dtos;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using FluentValidation;
namespace Application.Validation;


public class EditUserValidator : AbstractValidator<EditUserRequestDto>
{
    private readonly IUsersRepository userRepository;


    public EditUserValidator(IUsersRepository userRepository)
    {
        this.userRepository = userRepository;

        RuleFor(model => model.LastName).NotEmpty().WithMessage("Please specify a LastName");
        RuleFor(x => x.LastName)
           .Must((Name) =>
            {
                User? existingName = userRepository.GetByName(Name);

                if (existingName == null)
                {
                    return true;
                }

                return false;
            }).WithMessage("Name must be unique");
    }

}
