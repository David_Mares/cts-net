namespace Domain.Entities;

public class Benefit
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;
    public DateTime CreatedDate { get; set; }
    public DateTime LastModifiedDate { get; set; } = DateTime.Now;
    public MeasurementUnit? Unit { get; set; } = null;

}
