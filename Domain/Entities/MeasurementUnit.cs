namespace Domain.Entities;

public enum MeasurementUnit
{
    Other,
    Days,
    Months,
    Currency,

}