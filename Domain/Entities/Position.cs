namespace Domain.Entities;

public class Position 
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;
    public DateTime CreatedDate { get; set; }
    public DateTime LastModifiedDate { get; set; } = DateTime.Now;

       public virtual List<User>? Users { get; set; }



}