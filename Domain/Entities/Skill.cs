namespace Domain.Entities;

public class Skill 
{
    public int Id { get; set; }
    public string Description { get; set; } = string.Empty;
    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;
    public DateTime CreatedDate { get; set; }
    public DateTime LastModifiedDate { get; set; } = DateTime.Now;


}