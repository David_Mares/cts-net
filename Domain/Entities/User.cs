using System.ComponentModel.DataAnnotations;

namespace Domain.Entities;

public class User
{
    public int Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public List<Position>? Positions { get; set; }
    public List<Role>? Roles { get; set; }

    public bool Default { get; set; } = false;
    public bool Status { get; set; } = true;
    public DateTime CreatedDate { get; set; }
    public DateTime LastModifiedDate { get; set; } = DateTime.Now;


}