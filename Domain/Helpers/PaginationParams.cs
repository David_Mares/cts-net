namespace Domain.Helpers;

public class PaginationParams
{
    private const int MaxPageSize = 20;
    public int PageNumber { get; set; } = 1;
    private int _pageSize = 5;
    public int PageSize
    {
        get => _pageSize;
        set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
    }

    public string FilterString { get; set; } = string.Empty;
    public string OrderBy { get; set; } = string.Empty;
    public string Direction { get; set; } = string.Empty;
}
