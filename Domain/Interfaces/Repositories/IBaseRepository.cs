
using Domain.Helpers;

namespace Domain.Interfaces.Repositories
{
    public interface IBaseRepository<T>
    {
        Task<PagedList<T>> GetAll(PaginationParams paginationParams);

        Task<T?> GetOne(int id);
        Task<T> Insert(T entity);
        Task Edit(T entity);
        Task Delete(T entity);
    }
}