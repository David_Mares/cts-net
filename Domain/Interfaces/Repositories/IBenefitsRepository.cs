using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface IBenefitsRepository : IBaseRepository<Benefit>
{

    Benefit? GetByName(string name);
   
}