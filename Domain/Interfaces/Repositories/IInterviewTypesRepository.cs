using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface IInterviewTypesRepository : IBaseRepository<InterviewType>
{

    InterviewType? GetByDescription(string description);
   
}