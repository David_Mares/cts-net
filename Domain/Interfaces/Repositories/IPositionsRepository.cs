using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface IPositionsRepository : IBaseRepository<Position>
{

    Position? GetByName(string name);
   
}