using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface ISenioritiesRepository : IBaseRepository<Seniority>
{

    Seniority? GetByDescription(string description);
   
}