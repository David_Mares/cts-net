using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface ISkillsRepository : IBaseRepository<Skill>
{

    Skill? GetByDescription(string description);
   
}