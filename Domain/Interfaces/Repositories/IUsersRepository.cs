using Domain.Entities;

namespace Domain.Interfaces.Repositories;

public interface IUsersRepository : IBaseRepository<User>
{

    User? GetByName(string name);
    Task<List<Role>>? GetRoles(List<int> roleIds);
    Task<List<Position>>? GetPositions(List<int> positionIds);
}