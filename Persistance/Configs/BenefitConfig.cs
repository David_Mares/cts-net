using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class BenefitConfig : IEntityTypeConfiguration<Benefit> 
{
	public void Configure(EntityTypeBuilder<Benefit> builder) 
	{
		builder.ToTable("Benefits");
		builder.HasKey(c => c.Id);
	}
}
