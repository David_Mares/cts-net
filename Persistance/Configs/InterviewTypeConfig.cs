using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class InterviewTypeConfig : IEntityTypeConfiguration<InterviewType> 
{
	public void Configure(EntityTypeBuilder<InterviewType> builder) 
	{
		builder.ToTable("InterviewTypes");
		builder.HasKey(c => c.Id);
	}
}
