using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class PositionConfig : IEntityTypeConfiguration<Position> 
{
	public void Configure(EntityTypeBuilder<Position> builder) 
	{
		builder.ToTable("Positions");
		builder.HasKey(c => c.Id);
	}
}
