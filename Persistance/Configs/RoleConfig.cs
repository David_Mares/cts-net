using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class RoleConfig : IEntityTypeConfiguration<Role> 
{
	public void Configure(EntityTypeBuilder<Role> builder) 
	{
		builder.ToTable("Roles");
		builder.HasKey(c => c.Id);
	}
}
