using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class SeniorityConfig : IEntityTypeConfiguration<Seniority> 
{
	public void Configure(EntityTypeBuilder<Seniority> builder) 
	{
		builder.ToTable("Seniorities");
		builder.HasKey(c => c.Id);
	}
}
