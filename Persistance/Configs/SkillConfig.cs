using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class SkillConfig : IEntityTypeConfiguration<Skill> 
{
	public void Configure(EntityTypeBuilder<Skill> builder) 
	{
		builder.ToTable("Skills");
		builder.HasKey(c => c.Id);
	}
}
