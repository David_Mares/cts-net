using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Persistance.Configs;
class UserConfig : IEntityTypeConfiguration<User> 
{
	public void Configure(EntityTypeBuilder<User> builder) 
	{
		builder.ToTable("Users");
		builder.HasKey(c => c.Id);
	}
}
