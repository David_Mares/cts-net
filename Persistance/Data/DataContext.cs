using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Persistance.Configs;

namespace Persistance.Data;

public class DataContext : DbContext
{

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {

    }

    public DbSet<Skill> Skills => Set<Skill>();
    public DbSet<Benefit> Benefits => Set<Benefit>();
    public DbSet<Seniority> Seniorities => Set<Seniority>();
    public DbSet<InterviewType> InterviewTypes => Set<InterviewType>();
    public DbSet<Position> Positions => Set<Position>();
    public DbSet<User> Users => Set<User>();
      public DbSet<Role> Roles => Set<Role>();
    public DbSet<UserPosition> UserPositions => Set<UserPosition>();
    public DbSet<UserRole> UserRoles => Set<UserRole>();



    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfiguration(new SkillConfig());
        builder.ApplyConfiguration(new BenefitConfig());
        builder.ApplyConfiguration(new SeniorityConfig());
        builder.ApplyConfiguration(new InterviewTypeConfig());
        builder.ApplyConfiguration(new PositionConfig());
        builder.ApplyConfiguration(new UserConfig());
        builder.ApplyConfiguration(new RoleConfig());


           builder.Entity<User>()
                .HasMany(m => m.Roles)
                .WithMany(g => g.Users)
                .UsingEntity<UserRole>(
                    mg => mg.HasOne(prop => prop.Role).WithMany().HasForeignKey(prop => prop.RoleId),
                    mg => mg.HasOne(prop => prop.User).WithMany().HasForeignKey(prop => prop.UserId),
                    mg =>
                    {
                        mg.HasKey(prop => new { prop.RoleId, prop.UserId });
                    }
                );

                 builder.Entity<User>()
                .HasMany(m => m.Positions)
                .WithMany(g => g.Users)
                .UsingEntity<UserPosition>(
                    mg => mg.HasOne(prop => prop.Position).WithMany().HasForeignKey(prop => prop.PositionId),
                    mg => mg.HasOne(prop => prop.User).WithMany().HasForeignKey(prop => prop.UserId),
                    mg =>
                    {
                        mg.HasKey(prop => new { prop.PositionId, prop.UserId });
                    }
                );

        builder.Entity<UserPosition>()
            .ToTable("UserPositions");

        builder.Entity<UserRole>()
            .ToTable("UserRoles");


    }
}