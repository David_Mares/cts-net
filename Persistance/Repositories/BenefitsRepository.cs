
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class BenefitsRepository : IBenefitsRepository
{
    private readonly DataContext dataContext;

    public BenefitsRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }
    public async Task<PagedList<Benefit>> GetAll(PaginationParams paginationParams)
    {    IQueryable<Benefit> query = dataContext.Benefits.AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            query = query.Where(s => s.Name.Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }

        return await PagedList<Benefit>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }

      public async Task<Benefit?> GetOne(int id)
    {
        return await dataContext.Benefits.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    }


    public async Task<Benefit> Insert(Benefit newBenefit)
    {
        newBenefit.CreatedDate = DateTime.Now;
        var benefit = await dataContext.Benefits.AddAsync(newBenefit);
        await dataContext.SaveChangesAsync();
        return benefit.Entity;
    }
    public async Task Delete(Benefit benefitToDelete)
    {
        dataContext.Benefits.Remove(benefitToDelete);
        await dataContext.SaveChangesAsync();
    }

    public async Task Edit(Benefit currentBenefit)
    {
        currentBenefit.CreatedDate = (await GetOne(currentBenefit.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.Benefits.Update(currentBenefit);
        await dataContext.SaveChangesAsync();
    }


    public Benefit? GetByName(string name)
    {
        return dataContext.Benefits.AsNoTracking().FirstOrDefault(x => x.Name.ToLower() == name.ToLower());

    }

}