
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class InterviewTypesRepository : IInterviewTypesRepository
{
    private readonly DataContext dataContext;

    public InterviewTypesRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }

    public async Task<PagedList<InterviewType>> GetAll(PaginationParams paginationParams)
    {
        IQueryable<InterviewType> query = dataContext.InterviewTypes.AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            query = query.Where(s => s.Description.Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }

        return await PagedList<InterviewType>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }

    public InterviewType? GetByDescription(string description)
    {
        return dataContext.InterviewTypes.AsNoTracking().FirstOrDefault(x => x.Description.ToLower() == description.ToLower());

    }

    public async Task<InterviewType?> GetOne(int id)
    {
        return await dataContext.InterviewTypes.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<InterviewType> Insert(InterviewType newInterviewType)
    {
        newInterviewType.CreatedDate = DateTime.Now;
        var interviewType = await dataContext.InterviewTypes.AddAsync(newInterviewType);
        await dataContext.SaveChangesAsync();
        return interviewType.Entity;
    }

    public async Task Delete(InterviewType interviewTypeToDelete)
    {
        dataContext.InterviewTypes.Remove(interviewTypeToDelete);
        await dataContext.SaveChangesAsync();
    }

    public async Task Edit(InterviewType currentInterviewType)
    {
        currentInterviewType.CreatedDate = (await GetOne(currentInterviewType.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.InterviewTypes.Update(currentInterviewType);
        await dataContext.SaveChangesAsync();
    }
}