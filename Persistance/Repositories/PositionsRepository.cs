
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class PositionsRepository : IPositionsRepository
{
    private readonly DataContext dataContext;

    public PositionsRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }



    public async Task<PagedList<Position>> GetAll(PaginationParams paginationParams)
    {
        IQueryable<Position> query = dataContext.Positions.AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            query = query.Where(s => s.Description.Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }

        return await PagedList<Position>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }

    public Position? GetByName(string name)
    {
        return dataContext.Positions.AsNoTracking().FirstOrDefault(x => x.Name.ToLower() == name.ToLower());

    }

    public async Task Delete(Position PositionToDelete)
    {
        dataContext.Positions.Remove(PositionToDelete);
        await dataContext.SaveChangesAsync();
    }

    public async Task Edit(Position currentPosition)
    {
        currentPosition.CreatedDate = (await GetOne(currentPosition.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.Positions.Update(currentPosition);
        await dataContext.SaveChangesAsync();
    }
    public async Task<Position?> GetOne(int id)
    {
        return await dataContext.Positions.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

    }

    public async Task<Position> Insert(Position newPosition)
    {
        newPosition.CreatedDate = DateTime.Now;
        var position = await dataContext.Positions.AddAsync(newPosition);
        await dataContext.SaveChangesAsync();
        return position.Entity;
    }


}