
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class SenioritiesRepository : ISenioritiesRepository
{
    private readonly DataContext dataContext;

    public SenioritiesRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }

    public async Task<PagedList<Seniority>> GetAll(PaginationParams paginationParams)
    {
        IQueryable<Seniority> query = dataContext.Seniorities.AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            query = query.Where(s => s.Description.Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }

        return await PagedList<Seniority>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }

    public Seniority? GetByDescription(string description)
    {
        return dataContext.Seniorities.AsNoTracking().FirstOrDefault(x => x.Description.ToLower() == description.ToLower());

    }

    public async Task<Seniority?> GetOne(int id)
    {
        return await dataContext.Seniorities.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<Seniority> Insert(Seniority newSeniority)
    {
        newSeniority.CreatedDate = DateTime.Now;
        var seniority = await dataContext.Seniorities.AddAsync(newSeniority);
        await dataContext.SaveChangesAsync();
        return seniority.Entity;
    }

    public async Task Delete(Seniority seniorityToDelete)
    {
        dataContext.Seniorities.Remove(seniorityToDelete);
        await dataContext.SaveChangesAsync();
    }

    public async Task Edit(Seniority currentSeniority)
    {
        currentSeniority.CreatedDate = (await GetOne(currentSeniority.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.Seniorities.Update(currentSeniority);
        await dataContext.SaveChangesAsync();
    }
}