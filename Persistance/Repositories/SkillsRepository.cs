
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class SkillsRepository : ISkillsRepository
{
    private readonly DataContext dataContext;

    public SkillsRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }

    public async Task<PagedList<Skill>> GetAll(PaginationParams paginationParams)
    {
        IQueryable<Skill> query = dataContext.Skills.AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            query = query.Where(s => s.Description.Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }

        return await PagedList<Skill>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }
    public async Task<Skill?> GetOne(int id)
    {
        return await dataContext.Skills.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    }
    public async Task<Skill> Insert(Skill newSkill)
    {
        newSkill.CreatedDate = DateTime.Now;
        var skill = await dataContext.Skills.AddAsync(newSkill);
        await dataContext.SaveChangesAsync();
        return skill.Entity;
    }
    public async Task Edit(Skill currentSkill)
    {
        currentSkill.CreatedDate = (await GetOne(currentSkill.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.Skills.Update(currentSkill);
        await dataContext.SaveChangesAsync();
    }

    public async Task Delete(Skill skillToDelete)
    {
        dataContext.Skills.Remove(skillToDelete);
        await dataContext.SaveChangesAsync();
    }

    public Skill? GetByDescription(string description)
    {
        return dataContext.Skills.AsNoTracking().FirstOrDefault(x => x.Description.ToLower() == description.ToLower());
    }


}