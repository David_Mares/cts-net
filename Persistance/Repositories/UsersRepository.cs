
using Domain.Interfaces.Repositories;
using Domain.Entities;
using Persistance.Data;
using Microsoft.EntityFrameworkCore;
using Domain.Helpers;
using System.Linq.Dynamic.Core;

namespace Persistance.Repositories;


public class UsersRepository : IUsersRepository
{
    private readonly DataContext dataContext;

    public UsersRepository(DataContext dataContext)
    {
        this.dataContext = dataContext;
    }



    public async Task<PagedList<User>> GetAll(PaginationParams paginationParams)
    {
        IQueryable<User> query = dataContext.Users.Include(u => u.Roles).Include(u => u.Positions).AsQueryable();

        if (!string.IsNullOrEmpty(paginationParams.FilterString))
        {
            // query = query.Where(s => s.FirstName.Contains(paginationParams.FilterString));
            query = query.Where(s => (s.FirstName + " " + s.LastName).Contains(paginationParams.FilterString));
        }
        if (!string.IsNullOrEmpty(paginationParams.OrderBy))
        {
            if (paginationParams.Direction == "desc" || string.IsNullOrEmpty(paginationParams.Direction))
            {
                query = query.OrderBy($"{paginationParams.OrderBy} desc");
            }
            else
            {
                query = query.OrderBy(paginationParams.OrderBy);
            }
        }




        return await PagedList<User>.CreateAsync(query.AsNoTracking(), paginationParams.PageNumber, paginationParams.PageSize);
    }

    public User? GetByName(string name)
    {
        return dataContext.Users.AsNoTracking().FirstOrDefault(x => x.FirstName.ToLower() == name.ToLower());

    }

    public async Task Delete(User UserToDelete)
    {
        dataContext.Users.Remove(UserToDelete);
        await dataContext.SaveChangesAsync();
    }

    public async Task Edit(User currentUser)
    {
        currentUser.CreatedDate = (await GetOne(currentUser.Id))?.CreatedDate ?? DateTime.Now;
        dataContext.Users.Update(currentUser);
        await dataContext.SaveChangesAsync();
    }
    public async Task<User?> GetOne(int id)
    {
        return await dataContext.Users.Include(u => u.Roles).Include(u => u.Positions).AsNoTracking()
        .FirstOrDefaultAsync(x => x.Id == id);

    }

    public async Task<User> Insert(User newUser)
    {
        newUser.CreatedDate = DateTime.Now;

        var user = await dataContext.Users.AddAsync(newUser);
        await dataContext.SaveChangesAsync();
        return user.Entity;
    }

    public async Task<List<Role>>? GetRoles(List<int> roleIds)
    {
        List<Role> roles = await dataContext.Roles
                                .Where(r => roleIds.Contains(r.Id))
                                .ToListAsync();

        return roles;
    }
    public async Task<List<Position>>? GetPositions(List<int> positionIds)
    {
        List<Position> positions = await dataContext.Positions
                                .Where(r => positionIds.Contains(r.Id))
                                .ToListAsync();

        return positions;
    }

}